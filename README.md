# Présentation

Créer un perso dans Naheulbeuk, c'est compliqué.
On lance des dés, on prend des notes, on compare les valeurs dans des tableaux bordéliques, on prend des notes, on lance des dés, on prend des notes…
Lancer des dés c'est marrant. Prendre des notes et faire des calculs, moins.
Alors oui, il y a déjà des tonnes d'outils qui permettent de tout faire à notre place, mais…

- Ils sont moches (attention ça clashe).
- Ils ne sont pas modifiables à souhait.
- Ils ne sont pas faits en LaTeX.

Ce projet est là pour palier (à?) ces problèmes (ou au moins les deux derniers).
C'est peut-être pas encore très *user-friendly*, mais des améliorations sont au programme.
À l'avenir, on pourra envisager de produire directement une fiche de personnage pré-remplie à partir de ce programme.




# Notes

Date de dernière mise à jour : 31/07/2017

Programme réalisé en LaTeX. Pour toutes remarques : neiron.sdf (at) gmail.com

Les données reprises dans ce document sont les données officielles disponibles sur [naheulbeuk.com](https://www.naheulbeuk.com/) : *Origines et métiers des héros*, v. 3.1.

Pour votre sécurité, mangez cinq fruits et légumes par jour.




# Comment ça marche

Dans l'état actuel des choses, il est nécessaire d'avoir au moins des bases en LaTeX pour pouvoir utiliser ce programme.
Au moins savoir compiler un fichier, reconnaître un environnement, une macro…

## Installation

### Premier cas : Je sais utiliser `Git`

```
git clone git@bitbucket.org:NeironSDF/naheulbeuk-quickstart.git
```

### Second cas : Je suis quelqu'un de normal

Il y a un onglet `Downloads` dans le panneau à gauche.
Dedans, cliquer sur `Download repository`, enregistrer le `.zip` et le dé-zipper.


## Lancement

Le seul fichier à toucher est le `Creator.tex`.
Il va falloir l'éditer, le compiler, le ré-éditer, le re-compiler, etc.

Compiler avec `XeLaTeX` ou `LuaLaTeX`. `pdfLaTeX` ne gère pas les polices d'écritures au choix.

Une fonctionnalité permet d'afficher des liens hyper-textes, à l'aide du package `pdfcomment`.
Elle est désactivée par défaut car ça alourdit un peu la compilation.
Pour l'activer, il y a juste une option à activer, le programme s'occupe du reste.
Quelques trucs à prendre en compte :

- Il faut compiler le fichier deux voire trois fois de suite pour que `pdfcomment` fasse son effet.
- Tous les lecteurs de PDF n'affichent pas les commentaires.
- En particulier, si vous utilisez `TeXworks`, cette fonctionnalité peut faire planter le `Previewer`.


## Options

Elles s'ajoutent entre crochets, séparées par des virgules, dans la ligne :

```
\usepackage[<options>]{Quickstart}
```

- `pdfcomment` : ajouter des détails via commentaires pdf (voir ci-dessus)

- `soldats` : ajouter l'extension « Soldats »

- `supplements` : ajouter l'extension « Suppléments » (sbire, bourreau, nain de la mafia, amazone syldérienne)


## Le contenu

La plupart des commandes sont rappelées en commentaire dans le fichier `.tex` à modifier.


### Création d'un personnage

Toutes les commandes personnalisées doivent être effectuées à l'intérieur d'un environnement `creation`.
On peut utiliser plusieurs fois l'environnement, pour créer plusieurs personnages.


### Lancer des dés

Dans l'ordre qu'on veut :

1. Caractéristiques : 5 dés 6

    ```
    \caracteristiques abcde
    ```
    
    où `a`, `b`, `c`, `d`, `e` sont les valeurs brutes obtenues.
    
    Compilez, vous aurez la liste des origines et métiers possibles avec ces caractéristiques.

2. Argent : 2 dés 6

    ```
    \argent xy
    ```
    
    où `x` et `y` sont les valeurs brutes obtenues.

3. Points de destin : 1 dé 4

    ```
    \destin z
    ```
    
    où `z` est la valeur brute obtenue.

**Cas particulier :**

- Adresse très élevée ou très faible
    
    Une adresse supérieure ou égale à 13 donne un point d'AT/PRD.
    Inversement si inférieure ou égale à 8.
    
    **Exception :** Les soldats ne sont pas concernés.
    
    Ajouter `+`/`-` `AT`/`PRD` après `\caracteristiques` et les 5 valeurs.
    Exemple :
    
    ```
    \caracteristiques 66666+AT
    ```

- Intelligence ou force très élevée ou très faible
    
    Rien à faire, c'est déjà pris en compte.


### Choix d'une origine

Au choix parmi :

```
\barbare
\demielfe
\demiorque
\elfenoir
\elfesylvain
\gnome
\gobelin
\hautelfe
\humain
\nain
\ogre
\orque
\semihomme
```

Plusieurs variantes (avec ou sans espace, etc.) ou synonymes ont été pris en compte,
mais je détaille pas tout, la flemme.

**Cas particuliers :**

- Ogre

    L'ogre peut sacrifier jusqu'à 3 points d'attaque/parade contre des dégâts supplémentaires.
    
    Ajouter `-nAT` **et/ou** `-nPRD`, dans un ordre quelconque, après l'appel de `\ogre`.
    Exemple :
    
    ```
    \ogre -1AT-2PRD
    ```


### Choix d'un métier

Au choix parmi :

```
\assassin
\guerrier
\ingenieur
\mage
\marchand
\menestrel
\noble
\paladin
\pirate
\pretre
\ranger
\voleur
\officier
\eclaireur
\soldatlourd
\medecin
\armurier
\sbire
\bourreau
```

Variantes, idem.

L'officier, l'éclaireur, le soldat lourd, le médecin et l'armurier ne sont disponibles qu'avec l'option `soldats`.
Le sbire et le bourreau ne sont disponibles qu'avec l'option `supplements`.

**Cas particuliers :**

- Noble

    Le noble peut relancer les dés de sa fortune et additionner les résultats.
    
    Ajouter `+xy` après `\argent` et les deux premières valeurs.
    Exemple :
    ```
    \argent 12+34
    ```

- Guerrier

    Le guerrier peut échanger de l'attaque contre de la parade.
    
    Ajouter `+`/`-` `AT`/`PRD`, après l'appel de `\guerrier`.
    Exemple :
    
    ```
    \guerrier +PRD
    ```

- Marchand

    Le marchand perd un point d'AT/PRD et gagne un point d'INT/CHA.
    
    Ça va, je vais pas toutes vous les faire.

- Ranger

    Le ranger peut transférer un point d'une caractéristique de base dans une autre.

- Ingénieur

    L'ingénieur perd un point d'AT/PRD et gagne un point d'INT/AD.
    
    D'autre part, l'ingénieur peut choisir parmi plusieurs spécialités :
    
    ```
    \mecanicien
    \forgeron
    \ebeniste
    \cuir
    \couturier
    \cuisinier
    \alchimiste
    \medecine
    ```
    
    Un gobelin devient automatiquement "ingénieur gobelin", mais on peut le changer
    avec une des spécialités ci-dessus.
    
    **Attention :** Avec l'ajout de l'extension « Soldats », la macro `\medecin` désigne maintenant le métier de soldat médecin.
    À ne pas confondre avec `\medecine`, la spécialité de l'ingénieur !

- Mage
    
    Le mage peut choisir parmi plusieurs spécialités :
    
    ```
    \combat
    \domestique
    \feu
    \metamorphoses
    \thermodynamique
    \invocation
    \necromancie
    \illusion
    \eau
    \terre
    \air
    \noire
    ```

- Prêtre
    
    Le prêtre peut choisir parmi plusieurs dieux :
    
    ```
    \dlul
    \youclidh
    \slanoush
    \adathie
    ```

- Paladin

    Le paladin peut choisir parmi plusieurs dieux :
    
    ```
    \dlul
    \braav
    \slanoush
    \khornettoh
    ```


### Alternative : combo origine+métier

Au choix parmi :

```
\naindelamafia
\amazone
```

Ces choix ne sont disponibles qu'avec l'option `supplements`.


### Compétences

Les compétences héritées devraient apparaître directement,
ainsi que les autres compétences disponibles (sauf pour l'humain (sauf s'il n'a pas de métier)).

Les compétences sont désignées par leur numérotation dans le document *Résumé des Compétences*, v. 2.0.
**Ce n'est pas dans l'ordre alphabétique.**
L'ordre est redonné en annexe de ce document.

Pour choisir deux compétences :

```
\developper{x}{y}
```

où `x` et `y` sont les **numéros** des compétences.


### Intervention MJique

Ce programme **ne vérifie pas** si les consignes sont respectées,
tout simplement parce que le MJ peut avoir envie de les modifier pour une raison ou une autre.
De plus, même si des choix ne sont pas proposés, ils sont quand même disponibles.
Si ça ne suffit pas, le MJ a le droit de faire des ajustements :

- Modifier les caractéristiques de base
    
    Les noms des commandes sont assez parlants.
    
    ```
    \echanger{x}{y}
    ```
    
    ```
    \augmenter{x}
    ```
    
    ```
    \nerfer{x}
    ```

- Ajouter des compétences gratos

    ```
    \apprendre{x}
    ```
    
    où `x` est le **numéro** de la compétence.

- Ajouter des compétences inventées
    
    ```
    \decouvrir{x}
    ```
    
    où `x` est le **nom complet** de la compétence.

- Virer les messages sur les choix optionnels
    
    Par exemple, si le ranger est très content avec ses caractéristiques actuelles,
    un petit `\ok` virera le message lui proposant d'échanger des points.
    
    (Ça vire **tous** les messages optionnels, donc à n'utiliser qu'à la fin, pour nettoyer.)




# TODO

- Plus d'hyper-texte ?
- Ajouter des options au package `Quickstart`
- Les couleurs c'est pour les noobs.
- Synchroniser avec la fiche de perso auto-complétée




# Annexes

## Liste des compétences classiques

    1. Ambidextrie
    2. Agoraphobie
    3. Appel des renforts
    4. Appel du sauvage
    5. Appel du tonneau
    6. Appel du ventre
    7. Armes de bourrin
    8. Arnaque et carambouille
    9. Attire les monstres
    10. Bourre-pif
    11. Bricolo du dimanche
    12. Chance du rempailleur
    13. Chef de groupe
    14. Chercher des noises
    15. Chevaucher
    16. Chouraver
    17. Comprendre les animaux
    18. Cuistot
    19. Déplacement silencieux
    20. Désamorcer
    21. Détecter
    22. Érudition
    23. Escalader
    24. Forgeron
    25. Frapper lâchement
    26. Fariboles
    27. Fouiller dans les poubelles
    28. Instinct de survie
    29. Instinct du trésor
    30. Intimider
    31. Jonglage et danse
    32. Langues des monstres
    33. Méfiance
    34. Mendier et pleurnicher
    35. Nager
    36. Naïveté touchante
    37. Pénible
    38. Pister
    39. Premiers soins
    40. Radin
    41. Récupération
    42. Ressemble à rien
    43. Runes bizarres
    44. Sentir des pieds
    45. Serrurier
    46. Tête vide
    47. Tirer correctement
    48. Tomber dans les pièges
    49. Truc de mauviette


## Compétences spéciales

Ce sont les compétences spécifiques à certains métiers.
Elles ne sont pas dans la liste initiale mais elles ont quand même un numéro ici.
On peut ainsi y accéder tout le temps.

### Sbire

    50. Misérable
    51. Survivaliste
    52. Pleutre

### Bourreau 

    53. Tortionaire
    54. Poigne de fer
    55. Infâme

### Nain de la Mafia

    61. Roublard
    62. Bond d'attaque

### Amazone Syldérienne

    57. Armes longues
    58. Survivante
    59. Beauté féroce
    60. Ignorante
